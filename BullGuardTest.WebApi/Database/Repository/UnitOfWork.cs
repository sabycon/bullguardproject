﻿using BullGuardTest.Database.Repository.Interfaces;
using BullGuardTest.DataBase.Contexts;
using BullGuardTest.DataBase.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Database.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly InMemoryDbContext _ctx;
        public IProductRepository Products { get; }
        public IUserRepository Users { get; }
        public UnitOfWork(InMemoryDbContext ctx, IProductRepository productRepository, IUserRepository userRepository)
        {
            _ctx = ctx;
            Products = productRepository;
            Users = userRepository;
        }

        public Task<int> Save()
        {
            return _ctx.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _ctx.Dispose();
            }
        }
    }
}
