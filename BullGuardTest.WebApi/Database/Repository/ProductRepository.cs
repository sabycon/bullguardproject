﻿using BullGuardTest.Database.Repository;
using BullGuardTest.DataBase.Contexts;
using BullGuardTest.DataBase.DbModels;
using BullGuardTest.DataBase.Repository.Interfaces;
using BullGuardTest.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.DataBase.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(InMemoryDbContext ctx) : base(ctx)
        {

        }

        public async Task<IEnumerable<int>> GetAllIds()
        {
            return await _ctx.Products.Select(product => product.ID).ToListAsync();
        }

        public async Task<IEnumerable<Product>> Upgrade(Product product)
        {
            List<Product> productsForUpgrade = await _ctx.Products.Where(prod => (prod.SubscriptionPeriod == product.SubscriptionPeriod && prod.Devices > product.Devices)
                                    || (prod.SubscriptionPeriod > product.SubscriptionPeriod && prod.Devices >= product.Devices)).ToListAsync();

            return productsForUpgrade;
        }
    }
}
