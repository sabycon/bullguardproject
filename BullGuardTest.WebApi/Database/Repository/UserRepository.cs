﻿using BullGuardTest.Database.Repository.Interfaces;
using BullGuardTest.DataBase.Contexts;
using BullGuardTest.DataBase.DbModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Database.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(InMemoryDbContext ctx) : base(ctx)
        {

        }

        public async Task<User> GetByUsername(string username)
        {
            return await _ctx.Users.FirstOrDefaultAsync(user => user.UserName == username);
        }
    }
}
