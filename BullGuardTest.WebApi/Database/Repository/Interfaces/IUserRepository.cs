﻿using BullGuardTest.DataBase.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Database.Repository.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        public Task<User> GetByUsername(string username);
    }
}
