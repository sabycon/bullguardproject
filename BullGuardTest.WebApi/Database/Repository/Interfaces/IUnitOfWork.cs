﻿using BullGuardTest.DataBase.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Database.Repository.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        public IProductRepository Products { get; }
        public IUserRepository Users { get; }

        Task<int> Save();
    }
}
