﻿using BullGuardTest.Database.Repository.Interfaces;
using BullGuardTest.DataBase.Contexts;
using BullGuardTest.DataBase.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.DataBase.Repository.Interfaces
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<IEnumerable<Product>> Upgrade(Product product);
        Task<IEnumerable<int>> GetAllIds();
    }
}
