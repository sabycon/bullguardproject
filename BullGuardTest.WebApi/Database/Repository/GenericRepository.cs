﻿using BullGuardTest.Database.Repository.Interfaces;
using BullGuardTest.DataBase.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Database.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly InMemoryDbContext _ctx;

        public GenericRepository(InMemoryDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task Add(T Model)
        {
            await _ctx.Set<T>().AddAsync(Model);
        }

        public async Task<T> GetById(int id)
        {
            return await _ctx.Set<T>().FindAsync(id);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _ctx.Set<T>().ToListAsync();
        }

        public void Update(T model)
        {
            _ctx.Set<T>().Update(model);
        }
    }
}
