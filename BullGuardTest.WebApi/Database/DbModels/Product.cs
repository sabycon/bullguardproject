﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.DataBase.DbModels
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int SubscriptionPeriod { get; set; }
        public int Devices { get; set; }
        public decimal Price { get; set; }
    }
}
