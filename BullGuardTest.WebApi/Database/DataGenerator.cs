﻿using BullGuardTest.DataBase.Contexts;
using BullGuardTest.DataBase.DbModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.DataBase
{
    public class DataGenerator
    {
        public static void InitialSeed(IServiceProvider serviceProvider)
        {
            using (var ctx = new InMemoryDbContext(serviceProvider.GetRequiredService<DbContextOptions<InMemoryDbContext>>()))
            {
                if (ctx.Products.Any() || ctx.Users.Any())
                {
                    //data is already added
                    return;
                }

                //add initial items
                ctx.Products.AddRange(
                    new Product
                    {
                        ID = 1,
                        SubscriptionPeriod = 1,
                        Devices = 3,
                        Price = 60
                    },

                    new Product
                    {
                        ID = 2,
                        SubscriptionPeriod = 2,
                        Devices = 3,
                        Price = 90
                    },

                    new Product
                    {
                        ID = 3,
                        SubscriptionPeriod = 2,
                        Devices = 10,
                        Price = 130
                    },

                    new Product
                    {
                        ID = 4,
                        SubscriptionPeriod = 3,
                        Devices = 3,
                        Price = 120
                    }
                    );

                ctx.Users.AddRange(
                    new User
                    {
                        Id = 1,
                        Name = "BullGuard",
                        Email = "bullguard@example.com",
                        UserName = "test",
                        Password = "test123"
                    }
                    );

                ctx.SaveChanges();
            }
        }
    }
}
