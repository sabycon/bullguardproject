﻿using BullGuardTest.DataBase.DbModels;
using Microsoft.EntityFrameworkCore;

namespace BullGuardTest.DataBase.Contexts
{
    public class InMemoryDbContext : DbContext
    {
        public InMemoryDbContext(DbContextOptions<InMemoryDbContext> options)
        : base(options) { }

        public DbSet<Product> Products { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
