﻿using BullGuardTest.Database.Repository.Interfaces;
using BullGuardTest.DataBase.DbModels;
using BullGuardTest.DTO;
using BullGuardTest.Infrastructure.Mappers;
using BullGuardTest.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<UserDto> GetByUsername(string username)
        {
            User user = await _unitOfWork.Users.GetByUsername(username);

            if (user == null)
            {
                //do some handling here in case user is not found
                return null;
            }

            return UserMapper.GetUserDto(user);
        }

        public async Task<bool> ValidateCredentials(string username, string password)
        {
            UserDto userDto = await GetByUsername(username);

            if(userDto == null)
            {
                //maybe do some logging here
                throw new ArgumentNullException("user not found");
            }

            return username.Equals(userDto.UserName) && password.Equals(userDto.Password);
        }
    }
}
