﻿using BullGuardTest.Database.Repository.Interfaces;
using BullGuardTest.DataBase.DbModels;
using BullGuardTest.DataBase.Repository.Interfaces;
using BullGuardTest.DTO;
using BullGuardTest.Infrastructure.Enums;
using BullGuardTest.Infrastructure.Mappers;
using BullGuardTest.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BullGuardTest.Services
{
    public class ProductService : IProductService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ProductService> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IConfiguration configuration, ILogger<ProductService> logger, IUnitOfWork unitOfWork)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<ProductDto> GetByID(int id)
        {
            Product product = await _unitOfWork.Products.GetById(id);

            if (product == null)
            {
                return null;
            }

            return ProductMapper.GetProductDto(product);
        }

        public async Task<ProductDto> Promote()
        {
            IEnumerable<int> allIds = await _unitOfWork.Products.GetAllIds();

            if (!allIds.Any())
            {
                return null;
            }

            //trying to generate some randomness
            DateTime now = DateTime.UtcNow;
            int seed = Convert.ToInt32((now.Millisecond * now.Millisecond) / 3.15);

            //maybe create Random service for mocking
            Random rnd = new Random(seed);
            int idOfProductToPromote = rnd.Next(allIds.First(), allIds.Last());

            int discountValue = Convert.ToInt32(_configuration["DiscountValue"]);

            Product product = await _unitOfWork.Products.GetById(idOfProductToPromote);
            product.Price -= (product.Price * discountValue) / 100;

            _unitOfWork.Products.Update(product);
            int saveResult = await _unitOfWork.Save();

            //this was done just to prove that product was updated and saved to DB
            Product productUpdated = await _unitOfWork.Products.GetById(idOfProductToPromote);

            return ProductMapper.GetProductDto(productUpdated);
        }

        public async Task<IEnumerable<ProductDto>> Upgrade(int id)
        {
            List<ProductDto> result = new List<ProductDto>();

            Product product = await _unitOfWork.Products.GetById(id);

            //returning an empty list means there is no content
            if (product == null)
            {
                return result;
            }

            IEnumerable<Product> products = await _unitOfWork.Products.Upgrade(product);

            //returning an empty list means there is no content
            if (!products.Any())
            {
                return result;
            }

            foreach (Product item in products)
            {
                result.Add(ProductMapper.GetProductDto(item));
            }

            return result;
        }

        /// <summary>
        /// This will return duplicate results because one product suitable for upgrade can also be randomly picked for promotion
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ProductDto>> PromoAndUpgrade(int id)
        {
            List<ProductDto> result = new List<ProductDto>();

            ProductDto productDto = await Promote();

            result.Add(productDto);

            IEnumerable<ProductDto> upgradedProducts = await Upgrade(id);

            result.AddRange(upgradedProducts.ToList());

            return result;
        }

        public async Task<ValidationResponse> Add(ProductDto product)
        {
            try
            {
                {
                    if (product == null)
                    {
                        throw new ArgumentNullException("product is Null");
                    }
                }

                //validaton
                if (!ValidateProduct(product))
                {
                    return ValidationResponse.Failed;
                }

                //ID will be put by repository
                Product prod = ProductMapper.GetProduct(product);

                await _unitOfWork.Products.Add(prod);
                await _unitOfWork.Save();
                return ValidationResponse.Success;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error when adding product", ex);
                throw;
            }
        }

        public async Task<IEnumerable<ProductDto>> GetAll()
        {
            List<ProductDto> result = new List<ProductDto>();
            IEnumerable<Product> products = await _unitOfWork.Products.GetAll();

            if(products == null)
            {
                return result;
            }

            foreach(Product product in products)
            {
                ProductDto productDto = ProductMapper.GetProductDto(product);
                result.Add(productDto);
            }

            return result;
        }

        #region Private Methods
        private bool ValidateProduct(ProductDto product)
        {
            //must be at least 1 device
            bool isDeviceVaild = product.Devices > 0;

            //subscription period must be at least 1 year
            bool isSubscriptionPeriodValid = product.SubscriptionPeriod > 0;

            //let's say it's a promo product with 0 price, but it cannot be negative
            bool isPriceValid = product.Price >= 0;

            return isDeviceVaild && isSubscriptionPeriodValid && isPriceValid;
        }
        #endregion
    }
}
