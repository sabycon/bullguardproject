﻿using BullGuardTest.DTO;
using BullGuardTest.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Services.Interfaces
{
    public interface IProductService
    {
        Task<ProductDto> GetByID(int id);
        Task<ProductDto> Promote();
        Task<IEnumerable<ProductDto>> Upgrade(int id);
        Task<IEnumerable<ProductDto>> PromoAndUpgrade(int id);

        Task<ValidationResponse> Add(ProductDto product);
        Task<IEnumerable<ProductDto>> GetAll();
    }
}
