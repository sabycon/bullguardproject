﻿using BullGuardTest.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Services.Interfaces
{
    public interface IUserService
    {
        public Task<UserDto> GetByUsername(string username);
        public Task<bool> ValidateCredentials(string username, string password);
    }
}
