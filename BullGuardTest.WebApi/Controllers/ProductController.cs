﻿using BullGuardTest.DTO;
using BullGuardTest.Infrastructure.Enums;
using BullGuardTest.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly ILogger<ProductController> _logger;

        public ProductController(IProductService productService, ILogger<ProductController> logger)
        {
            _productService = productService;
            _logger = logger;
        }

        // GET api/<ProductController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            ProductDto productDto = await _productService.GetByID(id);

            if (productDto == null)
            {
                return NotFound($"Product with id: {id} could not be found");
            }

            return Ok(productDto);
        }

        // GET api/<ProductController>/Promotion
        [HttpGet("Promotion")]
        public async Task<IActionResult> Promotion()
        {
            ProductDto productDto = await _productService.Promote();

            if(productDto == null)
            {
                return NotFound("Could not find product for promotion");
            }

            return Ok(productDto);
        }

        // GET api/<ProductController>/Upgrade/1
        [HttpGet("Upgrade/{id}")]
        public async Task<IActionResult> Upgrade(int id)
        {
            IEnumerable<ProductDto> productDtos = await _productService.Upgrade(id);

            if (productDtos == null || !productDtos.Any())
            {
                return NotFound("Could not find product for update");
            }

            return Ok(productDtos);
        }

        // GET api/<ProductController>/All/1
        [HttpGet("All/{id}")]
        public async Task<IActionResult> All(int id)
        {
            IEnumerable<ProductDto> productDtos = await _productService.PromoAndUpgrade(id);

            return Ok(productDtos);
        }


        // POST api/<ProductController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProductDto product)
        {
            try
            {
                ValidationResponse result = await _productService.Add(product);

                if (result == ValidationResponse.Failed)
                {
                    return BadRequest("Product is not valid");
                }

                //maybe should return created here
                return Ok("Email sent");
            }
            catch(Exception ex)
            {
                return new StatusCodeResult(500);
            }
        }

        // GET api/<ProductController>/Products
        [HttpGet("Products")]
        public async Task<IActionResult> GetAllProcuts()
        {
            IEnumerable<ProductDto> productDtos = await _productService.GetAll();

            return Ok(productDtos);
        }
    }
}
