﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Infrastructure.Enums
{
    public enum ValidationResponse
    {
        Success = 0,
        Failed = 1
    }
}
