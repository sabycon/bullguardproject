﻿using BullGuardTest.DataBase.DbModels;
using BullGuardTest.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Infrastructure.Mappers
{
    public static class ProductMapper
    {
        public static ProductDto GetProductDto(Product product)
        {
            return new ProductDto()
            {
                ID = product.ID,
                Devices = product.Devices,
                SubscriptionPeriod = product.SubscriptionPeriod,
                Price = product.Price
            };
        }

        public static Product GetProduct(ProductDto productDto)
        {
            return new Product()
            {
                ID = productDto.ID,
                Devices = productDto.Devices,
                SubscriptionPeriod = productDto.SubscriptionPeriod,
                Price = productDto.Price
            };
        }
    }
}
