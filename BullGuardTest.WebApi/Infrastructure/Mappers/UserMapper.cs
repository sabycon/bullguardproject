﻿using BullGuardTest.DataBase.DbModels;
using BullGuardTest.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.Infrastructure.Mappers
{
    public static class UserMapper
    {
        public static UserDto GetUserDto(User user)
        {
            return new UserDto
            {
                Name = user.Name,
                Email = user.Email,
                UserName = user.UserName,
                Password = user.Password,
            };
        }
    }
}
