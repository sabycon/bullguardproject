﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest.DTO
{
    public class ProductDto
    {
        public int ID { get; set; }
        public int SubscriptionPeriod { get; set; }
        public int Devices { get; set; }
        public decimal Price { get; set; }
    }
}
