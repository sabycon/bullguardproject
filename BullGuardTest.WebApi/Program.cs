using BullGuardTest.DataBase;
using BullGuardTest.DataBase.Contexts;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BullGuardTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // 1.Get the IWebHost which will host this application.
            IWebHost host = CreateWebHostBuilder(args).Build();

            //2. Find the service layer within our scope.
            using (IServiceScope scope = host.Services.CreateScope())
            {
                //3. Get the instance of BoardGamesDBContext in our services layer
                IServiceProvider services = scope.ServiceProvider;
                InMemoryDbContext context = services.GetRequiredService<InMemoryDbContext>();

                //4. Call the DataGenerator to create sample data
                DataGenerator.InitialSeed(services);
            }

            //Continue to run the application
            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
