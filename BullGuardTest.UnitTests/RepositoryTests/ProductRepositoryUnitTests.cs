using BullGuardTest.DataBase.Contexts;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore;
using BullGuardTest.DataBase.DbModels;
using BullGuardTest.DataBase.Repository.Interfaces;
using BullGuardTest.DataBase.Repository;
using System.Collections.Generic;
using System.Linq;

namespace BullGuardTest.UnitTests.RepositoryTests
{
    public class ProductRepositoryUnitTests
    {

        protected DbContextOptions<InMemoryDbContext> ContextOptions { get; } = new DbContextOptionsBuilder<InMemoryDbContext>().UseInMemoryDatabase("UnitTestDatabase").Options;

        [SetUp]
        public void Setup()
        {
            Seed();
        }

        [Test]
        public void Upgrade_returns_correct_product()
        {
            //Arrange
            Product product = new Product
            {
                ID = 2,
                SubscriptionPeriod = 2,
                Devices = 3,
                Price = 90
            };

            using (InMemoryDbContext ctx = new InMemoryDbContext(ContextOptions))
            {
                IProductRepository productRepository = new ProductRepository(ctx);

                //Act
                List<Product> actualResult = productRepository.Upgrade(product).Result.ToList();

                //Assert
                Product productThree = actualResult.FirstOrDefault(prod => prod.ID == 3);
                Product productFour = actualResult.FirstOrDefault(prod => prod.ID == 4);

                Assert.IsTrue(actualResult.Count == 2);
                Assert.IsTrue(productThree != null && productFour != null);
                Assert.IsTrue(productThree.SubscriptionPeriod  == product.SubscriptionPeriod && productThree.Devices > product.Devices);
                Assert.IsTrue(productFour.SubscriptionPeriod > product.SubscriptionPeriod && productFour.Devices >= product.Devices);
            }
        }

        [Test]
        public void Upgrade_returns_no_product()
        {
            //Arrange
            Product product = new Product
            {
                ID = 7,
                SubscriptionPeriod = 4,
                Devices = 15,
                Price = 90
            };

            using (InMemoryDbContext ctx = new InMemoryDbContext(ContextOptions))
            {
                IProductRepository productRepository = new ProductRepository(ctx);

                //Act
                List<Product> actualResult = productRepository.Upgrade(product).Result.ToList();

                //Assert
                Assert.IsTrue(!actualResult.Any());
            }
        }

        [TestCase(3)]
        public void Can_get_item(int id)
        {
            //Arrange           
            using (InMemoryDbContext ctx = new InMemoryDbContext(ContextOptions))
            {
                IProductRepository productRepository = new ProductRepository(ctx);

                //Act
                Product actualResult = productRepository.GetById(id).GetAwaiter().GetResult();

                //Assert
                Assert.IsTrue(actualResult != null);
                Assert.IsTrue(actualResult.SubscriptionPeriod == 2 && actualResult.Devices == 10);
            }
        }


        #region Private Methods
        private void Seed()
        {
            using (InMemoryDbContext ctx = new InMemoryDbContext(ContextOptions))
            {
                ctx.Database.EnsureDeleted();
                ctx.Database.EnsureCreated();

                //add initial items
                ctx.Products.AddRange(
                    new Product
                    {
                        ID = 3,
                        SubscriptionPeriod = 2,
                        Devices = 10,
                        Price = 130
                    },

                    new Product
                    {
                        ID = 4,
                        SubscriptionPeriod = 3,
                        Devices = 3,
                        Price = 120
                    },

                    new Product
                    {
                        ID = 5,
                        SubscriptionPeriod = 2,
                        Devices = 2,
                        Price = 150
                    }
                    );

                ctx.SaveChanges();
            }
        }
        #endregion
    }
}